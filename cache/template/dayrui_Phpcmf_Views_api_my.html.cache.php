<?php if ($fn_include = $this->_include("header.html")) include($fn_include); ?>

<div class="note note-danger">
    <p><?php echo dr_lang('添加快捷菜单可以出现在后台首页之中，方便快速进入管理'); ?></p>
</div>

<form action="" class="form-horizontal" method="post" name="myform" id="myform">
    <?php echo dr_form_hidden(); ?>
    <input type="hidden" id="dr_color" name="data[color]" value="<?php echo $admin['color']; ?>">
    <div class="row myfbody">
        <div class="col-md-12">

            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject font-green"><?php echo dr_lang('资料修改'); ?></span>
                    </div>
                </div>
                <div class="portlet-body form">

                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo dr_lang('密码'); ?></label>
                            <div class="col-md-9">
                                <label><input type="text" class="form-control input-large" name="password"></label>
                                <span class="help-block"> <?php echo dr_lang('留空表示不修改密码'); ?> </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label"><?php echo dr_lang('快捷链接'); ?></label>
                            <div class="col-md-9">
                                <div class="form-control-static">
                                    <a class="btn green btn-xs" title="<?php echo dr_lang('添加'); ?>" href="javascript:;" onClick="add_menu()"> <i class="fa fa-plus"></i> </a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <div id="menu_body">
                                    <?php if (!empty($admin['usermenu'])) {  if (is_array($admin['usermenu'])) { $count=count($admin['usermenu']);foreach ($admin['usermenu'] as $k=>$t) { ?>
                                    <div class="menu-sort">
                                        <label><i class="fa fa-arrows"></i></label><label><input class="form-control" type="text" name="data[usermenu][name][<?php echo $k; ?>]" value="<?php echo $t['name']; ?>" /></label><label><select class="form-control" name="data[usermenu][color][<?php echo $k; ?>]">
                                                <?php if (is_array($color)) { $count=count($color);foreach ($color as $tt) { ?>
                                                <option value="<?php echo $tt; ?>" <?php if ($tt == $t['color']) { ?> selected<?php } ?>> <?php echo $tt; ?> </option>
                                                <?php } } ?>
                                            </select></label><label><input class="form-control input-large" type="text" name="data[usermenu][url][<?php echo $k; ?>]" value="<?php echo $t['url']; ?>" /></label><label><a class="btn red btn-xs" title="<?php echo dr_lang('删除'); ?>" href="javascript:;" onClick="remove_menu(this)"> <i class="fa fa-trash"></i> </a></label>
                                    </div>
                                    <?php } }  } ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>

    </div>

    <div class="portlet-body form myfooter">
        <div class="form-actions text-center">
            <button type="button" onclick="dr_ajax_submit('<?php echo dr_now_url(); ?>', 'myform', '2000')" class="btn green"> <i class="fa fa-save"></i> <?php echo dr_lang('保存内容'); ?></button>
        </div>
    </div>
</form>

<script type="text/javascript">
    function add_menu() {
        var color = '<?php echo $select_color; ?>';
        var data = '<div class="menu-sort"><label><i class="fa fa-arrows"></i></label><label><input class="form-control" placeholder="<?php echo dr_lang('名称'); ?>" type="text" name="data[usermenu][name][]" /></label><label><select class="form-control" name="data[usermenu][color][]">'+color+'</select></label><label><input placeholder="<?php echo dr_lang('URL'); ?>" class="form-control  input-large" type="text" name="data[usermenu][url][]" /></label><label><a class="btn red btn-xs" title="<?php echo dr_lang('删除'); ?>" href="javascript:;" onClick="remove_menu(this)"> <i class="fa fa-trash"></i> </a></label></div>';
        $('#menu_body').append(data);
    }
    function remove_menu(_this) {
        $(_this).parent().parent().remove();
    }
    function set_color(o, file) {
        $('.btn-outline').removeClass('red');
        $(o).addClass('red');
        $(o).removeClass('default');
        parent.$('#style_color').attr("href", "<?php echo THEME_PATH; ?>assets/layouts/layout/css/themes/" + file + ".min.css");
        $('#style_color').attr("href", "<?php echo THEME_PATH; ?>assets/layouts/layout/css/themes/" + file + ".min.css");
        $("#dr_color").val(file);
    }
    $("#menu_body").sortable({
        items: "div"
    });
</script>
<?php if ($fn_include = $this->_include("footer.html")) include($fn_include); ?>