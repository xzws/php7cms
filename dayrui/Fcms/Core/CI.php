<?php namespace Phpcmf;

// 公共类
abstract class Common extends \CodeIgniter\Controller
{

    private static $instance;

    public $uid;
    public $admin;
    public $member;
    public $module;
    public $member_cache;
    public $member_authid;

    public $site; // 网站id信息
    public $site_info; // 网站配置信息
    public $site_domain; // 全部站点域名

    public $session; // 网站session对象


    /**
     * 初始化共享控制器
     */
    public function __construct(...$params)
    {
        parent::__construct(...$params);

        self::$instance =& $this;

        if (defined('IS_INSTALL')) {
            return;
        }

        // 站点配置
        if (is_file(WRITEPATH.'config/site.php')) {
            $this->site_info = require WRITEPATH.'config/site.php';
            foreach ($this->site_info as $id => $t) {
                !$t['SITE_DOMAIN'] && $t['SITE_DOMAIN'] = DOMAIN_NAME;
                $this->site[$id] = $id;
                $this->site_info[$id] = $t;
                $this->site_info[$id]['SITE_ID'] = $id;
                $this->site_info[$id]['SITE_URL'] = dr_http_prefix($t['SITE_DOMAIN'].'/');
                $this->site_info[$id]['SITE_MURL'] = dr_http_prefix(($t['SITE_MOBILE'] ? $t['SITE_MOBILE'] : $t['SITE_DOMAIN']).'/');
            }
        } else {
            $this->site_info[1] = [
                'SITE_ID' => 1,
                'SITE_URL' => dr_http_prefix(DOMAIN_NAME.'/'),
                'SITE_MURL' => dr_http_prefix(DOMAIN_NAME.'/'),
            ];
        }

        define('IS_NOT_301', 1);

        // 站点id
        $siteid = 1;
        $client = []; // 电脑域名对应的手机域名
        if (is_file(WRITEPATH.'config/domain_client.php')) {
            $client = require WRITEPATH.'config/domain_client.php';
        }

        $is_mobile = IS_ADMIN ? 0 : $this->_is_mobile();

        $this->site_domain = []; // 全网域名对应的站点id
        if (is_file(WRITEPATH.'config/domain_site.php')) {
            $this->site_domain = require WRITEPATH.'config/domain_site.php';
        }

        if (isset($this->site_domain[DOMAIN_NAME]) && isset($this->site_info[$this->site_domain[DOMAIN_NAME]])) {
            // 通过域名来获取siteid
            $siteid = (int)$this->site_domain[DOMAIN_NAME];
            // 通过域名判断是否移动端
            $client && in_array(DOMAIN_NAME, $client) && $is_mobile = true;
            // 判断是否是网站的"其他域名"

        }

        // 站点id
        define('SITE_ID', max((int)$siteid, 1));


        // 客户端识别
        define('IS_PC', !$is_mobile);
        define('IS_MOBILE', $is_mobile);

        // 后台域名
        define('ADMIN_URL', dr_http_prefix(DOMAIN_NAME.'/'));

        // 站点共享变量
        define('SITE_URL', $this->site_info[SITE_ID]['SITE_URL']);
        define('SITE_MURL', $this->site_info[SITE_ID]['SITE_MURL']);
        define('SITE_NAME', $this->site_info[SITE_ID]['SITE_NAME']);
        define('SITE_LOGO', $this->site_info[SITE_ID]['SITE_LOGO']);
        define('SITE_THEME', strlen($this->site_info[SITE_ID]['SITE_THEME']) ? $this->site_info[SITE_ID]['SITE_THEME'] : 'default');
        define('SITE_SEOJOIN', strlen($this->site_info[SITE_ID]['SITE_SEOJOIN']) ? $this->site_info[SITE_ID]['SITE_SEOJOIN'] : '_');
        define('SITE_REWRITE', (int)$this->site_info[SITE_ID]['SITE_REWRITE']);
        define('SITE_TEMPLATE', strlen($this->site_info[SITE_ID]['SITE_TEMPLATE']) ? $this->site_info[SITE_ID]['SITE_TEMPLATE'] : 'default');
        define('SITE_LANGUAGE', strlen($this->site_info[SITE_ID]['SITE_LANGUAGE']) ? $this->site_info[SITE_ID]['SITE_LANGUAGE'] : 'zh-cn');
        define('SITE_TIME_FORMAT', strlen($this->site_info[SITE_ID]['SITE_TIME_FORMAT']) ? $this->site_info[SITE_ID]['SITE_TIME_FORMAT'] : 'Y-m-d H:i:s');

        // 全局URL
        define('ROOT_URL', $this->site_info[1]['SITE_URL']); // 主站URL
        define('LANG_PATH', ROOT_URL.'config/language/'.SITE_LANGUAGE.'/'); // 语言包
        define('THEME_PATH', ROOT_URL.'static/'); // 站点风格
        define('HOME_THEME_PATH', THEME_PATH.SITE_THEME.'/'); // 站点风格

        // 本地附件上传目录和地址
        if (SYS_ATTACHMENT_PATH
            && (strpos(SYS_ATTACHMENT_PATH, '/') === 0 || strpos(SYS_ATTACHMENT_PATH, ':') !== false)
            && is_dir(SYS_ATTACHMENT_PATH)) {
            // 相对于根目录
            // 附件上传目录
            define('SYS_UPLOAD_PATH', rtrim(SYS_ATTACHMENT_PATH, DIRECTORY_SEPARATOR).'/');
            // 附件访问URL
            define('SYS_UPLOAD_URL', trim(SYS_ATTACHMENT_URL, '/').'/');
        } else {
            // 在当前网站目录
            $path = trim(SYS_ATTACHMENT_PATH ? SYS_ATTACHMENT_PATH : 'uploadfile', '/');
            // 附件上传目录
            define('SYS_UPLOAD_PATH', ROOTPATH.$path.'/');
            // 附件访问URL
            define('SYS_UPLOAD_URL', ROOT_URL.$path.'/');
        }

        // 用户系统
        $this->member_cache = $this->get_cache('member');
        define('MEMBER_URL', (IS_PC ? SITE_URL : SITE_MURL).'index.php?s=member');

        // 网站常量
        define('SITE_ICP', $this->get_cache('site', SITE_ID, 'config', 'SITE_ICP'));
        define('SITE_TONGJI', $this->get_cache('site', SITE_ID, 'config', 'SITE_TONGJI'));
        // 默认登录时间
        define('SITE_LOGIN_TIME', $this->member_cache['config']['logintime'] ? max(intval($this->member_cache['config']['logintime']), 500) : 36000);
        // 定义交易变量
        define('SITE_SCORE', dr_lang($this->member_cache['score'] ? $this->member_cache['score'] : '金币'));
        define('SITE_EXPERIENCE', dr_lang($this->member_cache['experience'] ? $this->member_cache['experience'] : '经验'));

        // 获取当前的登录记录
        // 正常验证
        $this->uid = (int)\Phpcmf\Service::M('member')->member_uid();
        $this->member = \Phpcmf\Service::M('member')->get_member($this->uid);
        // 验证账号cookie的有效性
        if (!\Phpcmf\Service::M('member')->check_member_cookie($this->member)) {
            $this->uid = 0;
            $this->member = [];
        }
        define('IS_API_AUTH_POST', 0);

        if (IS_ADMIN) {
            // 开启session
            $this->session();
            // 后台登录判断
            $this->admin = \Phpcmf\Service::M('auth')->is_admin_login($this->member);
            \Phpcmf\Service::V()->admin();
            \Phpcmf\Service::V()->assign([
                'admin' => $this->admin,
                'is_ajax' => \Phpcmf\Service::L('Input')->get('is_ajax'),
                'is_mobile' => $this->_is_mobile() ? 1 : 0,
            ]);


        }



        // 用户权限id
        $this->member_authid = $this->member ? $this->member['authid'] : [0];

        \Phpcmf\Service::V()->assign([
            'member' => $this->member,
        ]);


        define('SITE_FID', 0);

        // 挂钩点 程序初始化之后
        \Phpcmf\Hooks::trigger('cms_init');
    }

    /**
     * 开启session
     */
    public function session() {
        
        if ($this->session) {
            return $this->session;
        }
        
        $this->session = \Config\Services::session();
        $this->session->start();
        
        return $this->session;
    }

    /**
     * 读取缓存
     */
    public function get_cache(...$params) {
        return \Phpcmf\Service::L('cache')->get(...$params);
    }

    // 初始化模块
    protected function _module_init($dirname = '', $siteid = SITE_ID) {

        !$dirname && $dirname = APP_DIR;

        // 判断模块是否存在
        $this->module = \Phpcmf\Service::L('cache')->get('module-'.$siteid.'-'.$dirname);
        if (!$this->module) {
            $dirname == 'share' && IS_ADMIN && $this->_admin_msg(0, dr_lang('系统未安装共享模块，无法使用栏目'));
            IS_ADMIN && $this->_admin_msg(0, dr_lang('模块【%s】不存在', $dirname));
            $this->_msg(0, dr_lang('模块【%s】不存在', $dirname));
            return;
        }

        // 无权限访问模块
        if (!IS_ADMIN && !IS_MEMBER
            && !dr_member_auth($this->member_authid, $this->member_cache['auth_module'][$siteid][$dirname]['home'])) {
            $this->_msg(0, dr_lang('您的用户组无权限访问模块'));
            return;
        }

        // 初始化数据表
        $this->content_model = \Phpcmf\Service::M('Content', $dirname);
        $this->content_model->_init($dirname, $siteid);

        // 兼容老版本
        define('MOD_DIR', $dirname);
        define('IS_SHARE', $this->module['share']);
        define('IS_COMMENT', $this->module['comment']);
        define('MODULE_URL', IS_SHARE ? '/' : $this->module['url']); // 共享模块没有模块url
        define('MODULE_NAME', $this->module['name']);
        
        // 设置模板到模块下
        !IS_SHARE && \Phpcmf\Service::V()->module(MOD_DIR);
    }

    /**
     * 统一返回json格式并退出程序
     */
    public function _json($code, $msg, $data = []){

        echo json_encode(dr_return_data($code, $msg, $data));exit;
    }

    /**
     * 统一返回jsonp格式并退出程序
     */
    public function _jsonp($code, $msg, $data = []){
        
        $callback = dr_safe_replace(\Phpcmf\Service::L('Input')->get('callback'));
        !$callback && $callback = 'callback';
        
        echo $callback.'('.json_encode(dr_return_data($code, $msg, $data)).')';exit;
    }

    /**
     * 加载数组配置文件
     */
    public function _require_array($file) {

        if (!is_file($file)) {
            return [];
        }

        $array = require $file;

        return $array;
    }

    /**
     * 后台提示信息
     */
    public function _admin_msg($code, $msg, $url = '', $time = 3) {

        \Phpcmf\Service::L('Input')->get('callback') && exit($this->_jsonp($code, $msg));
        (\Phpcmf\Service::L('Input')->get('is_ajax') || IS_API_AUTH_POST || IS_AJAX) && exit($this->_json($code, $msg));

        $burl = $url ? $url : (isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], 'php?') !== false ? $_SERVER['HTTP_REFERER'] : '');

        if (!$code && $burl && strpos($burl, '&isback=') === false && strpos($burl, 'c=home&m=home') === false) {
            // 存在URL返回提示
            $burl = str_replace(array('&isback=', '&iscode=', $msg), '&', $burl).'&isback='.$msg.'&iscode='.$code;
            // 避免重复定向
            urldecode(FC_NOW_URL) != urldecode($burl) && dr_redirect($burl, 'auto');
        }

        if (!$url) {
            $backurl = $_SERVER['HTTP_REFERER'];
            !$backurl && $backurl = SELF;
        } else {
            $backurl = '';
        }

        // 不存在URL时进入提示页面
        \Phpcmf\Service::V()->assign([
            'msg' => $msg,
            'url' => \Phpcmf\Service::L('Input')->xss_clean($url),
            'time' => $time,
            'mark' => $code,
            'backurl' => $backurl,
            'is_msg_page' => 1,
        ]);
        
        \Phpcmf\Service::V()->display('msg.html', 'admin');
        exit;
    }

    /**
     * 前台提示信息
     */
    public function _msg($code, $msg, $url = '', $time = 3) {

        \Phpcmf\Service::L('Input')->get('callback') && exit($this->_jsonp($code, $msg));
        (\Phpcmf\Service::L('Input')->get('is_ajax') || IS_API_AUTH_POST || IS_AJAX) && exit($this->_json($code, $msg));

        if (!$url) {
            $backurl = $_SERVER['HTTP_REFERER'];
            !$backurl && $backurl = SELF;
        } else {
            $backurl = '';
        }

        \Phpcmf\Service::V()->assign([
            'msg' => $msg,
            'url' => \Phpcmf\Service::L('Input')->xss_clean($url),
            'time' => $time,
            'mark' => $code,
            'code' => $code,
            'backurl' => $backurl,
            'meta_title' => SITE_NAME
        ]);
        \Phpcmf\Service::V()->display('msg.html');
        !defined('SC_HTML_FILE') && exit();
    }


    /**
     * 附件信息
     */
    public function get_attachment($id) {

        $id = (int)$id;
        if (!$id) {
            return null;
        }

        $data = \Phpcmf\Service::L('cache')->init()->get('attach-info-'.$id);
        if ($data) {
            return $data;
        }

        $data = \Phpcmf\Service::M()->db->table('attachment')->where('id', $id)->get()->getRowArray();
        if (!$data) {
            return null;
        } elseif ($data['related']) {
            $info = \Phpcmf\Service::M()->db->table('attachment_data')->where('id', $id)->get()->getRowArray();
        } else {
            $info = \Phpcmf\Service::M()->db->table('attachment_unused')->where('id', $id)->get()->getRowArray();
        }

        if (!$info) {
            return null;
        }

        // 合并变量
        $info = $data + $info;

        // 文件真实地址
        $info['file'] = $info['remote'] && ($remote = $this->get_cache('attachment', $info['remote'])) ? $remote['value']['path'].$info['attachment'] : SYS_UPLOAD_PATH.$info['attachment'];

        // 附件属性信息
        $info['attachinfo'] = dr_string2array($info['attachinfo']);

        $info['url'] = dr_get_file_url($info);

        SYS_CACHE && SYS_CACHE_ATTACH && \Phpcmf\Service::L('cache')->init()->save('attach-info-'.$id, $info, SYS_CACHE_ATTACH * 3600);

        return $info;
    }

    /**
     * 引用404页面
     */
    protected function goto_404_page($msg) {
        \Phpcmf\Service::V()->assign([
            'msg' => $msg,
            'meta_title' => dr_lang('你访问的页面不存在')
        ]);
        \Phpcmf\Service::V()->display('404.html');
        !defined('SC_HTML_FILE') && exit();
    }

    /**
     * 生成静态时的跳转提示
     */
    protected function _html_msg($code, $msg, $url = '') {
        \Phpcmf\Service::V()->assign([
            'msg' => $msg,
            'url' => $url,
            'mark' => $code
        ]);
        \Phpcmf\Service::V()->display('html_msg.html', 'admin');exit;
    }

    /**
     * 后台登录判断
     */
    protected function _is_admin_login() {
        return \Phpcmf\Service::M('auth')->_is_admin_login();
    }

    /**
     * 获取用户组权限的积分及统计参数累计
     */
    public function _member_auth_value($authid, $name) {

        return 1;
    }

    /**
     * 获取网站用户的积分及统计参数累计
     */
    public function _member_value($authid, $value) {


        return 1;

    }

    /**
     * 获取模块栏目的积分及统计参数累计
     */
    public function _module_member_value($catid, $dir, $auth, $authid = [0]) {

        $value = $this->member_cache['auth_module'][SITE_ID][$dir]['category'][$catid][$auth];
        if (!$value) {
            return 0;
        }

        $this->_member_value($authid, $value);
    }
    
    /**
     * 判断模块栏目是否具有用户操作权限
     */
    public function _module_member_category($category, $dir, $auth) {

        if (!$category) {
            return [];
        }

        foreach ($category as $id => $t) {
            // 筛选可发布的栏目权限
            if (!$t['child']) {
                if ($t['mid'] != $dir) {
                    // 模块不符合 排除
                    unset($category[$id]);
                } elseif (!dr_member_auth($this->member_authid, $this->member_cache['auth_module'][SITE_ID][$dir]['category'][$t['id']][$auth])) {
                    // 用户的的权限判断
                    unset($category[$id]);
                }
            }
        }

        return $category;
    }

    /**
     * 判断后台uri是否具有操作权限
     */
    public function _is_admin_auth($uri = '') {
        return 1;
    }

    /**
     * 是否移动端访问访问
     */
    protected function _is_mobile() {

        if (isset ($_SERVER['HTTP_X_WAP_PROFILE'])) {
            // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
            return true;
        } elseif (isset ($_SERVER['HTTP_USER_AGENT'])) {
            // 判断手机发送的客户端标志,兼容性有待提高
            $clientkeywords = [
                'nokia',
                'sony',
                'ericsson',
                'mot',
                'samsung',
                'htc',
                'sgh',
                'lg',
                'sharp',
                'sie-',
                'philips',
                'panasonic',
                'alcatel',
                'lenovo',
                'iphone',
                'ipod',
                'blackberry',
                'meizu',
                'android',
                'netfront',
                'symbian',
                'ucweb',
                'windowsce',
                'palm',
                'operamini',
                'operamobi',
                'openwave',
                'nexusone',
                'cldc',
                'midp',
                'wap',
                'mobile'
            ];
            // 从HTTP_USER_AGENT中查找手机浏览器的关键字
            if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))){
                return true;
            }
        }
        // 协议法，因为有可能不准确，放到最后判断
        if (isset ($_SERVER['HTTP_ACCEPT'])) {
            // 如果只支持wml并且不支持html那一定是移动设备
            // 如果支持wml和html但是wml在html之前则是移动设备
            if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html'))))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Api认证匹配
     */
    protected function _api_auth() {

        $this->_json(0, 'NULL 关闭验证启用防盗机制');
    }


    /**
     * 插件的clink值
     */
    protected function _app_clink()
    {
        $data = [];

        return $data;
    }

    /**
     * 插件的cbottom值
     */
    protected function _app_cbottom()
    {
        $data = [];

        return $data;
    }

    /**
     * Get the CI singleton
     */
    public static function &get_instance()
    {
        return self::$instance;
    }

}

