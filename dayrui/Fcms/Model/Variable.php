<?php namespace Phpcmf\Model;

class Variable extends \Phpcmf\Model
{


    // 缓存
    public function cache() {

        $data = $this->table('var')->getAll();
        $cache = [];
        if ($data) {
            foreach ($data as $t) {
                $cache[$t['cname']] = $t['value'];
            }
        }

        \Phpcmf\Service::L('cache')->set_file('var', $cache);
    }
}