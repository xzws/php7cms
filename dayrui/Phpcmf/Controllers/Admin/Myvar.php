<?php namespace Phpcmf\Controllers\Admin;

class Myvar extends \Phpcmf\Common
{
	private $type;
	private $form; // 表单验证配置

	public function __construct(...$params) {
		parent::__construct(...$params);
		\Phpcmf\Service::V()->assign('menu', \Phpcmf\Service::M('auth')->_admin_menu(
			[
				'自定义变量' => [\Phpcmf\Service::L('Router')->class.'/index', 'fa fa-code'],
				'添加' => ['add:'.\Phpcmf\Service::L('Router')->class.'/add', 'fa fa-plus'],
                'help' => [357],
			]
		));
		// 表单验证配置
		$this->form = [
			'name' => [
				'name' => '名称',
				'rule' => [
					'empty' => dr_lang('名称不能为空')
				],
				'filter' => [],
				'length' => '200'
			],
			'cname' => [
				'name' => '别名',
				'rule' => [
					'empty' => dr_lang('别名不能为空')
				],
				'filter' => [],
				'length' => '200'
			],
		];
		$this->type = [
			0 => dr_lang('逻辑值'),
			1 => dr_lang('文本值'),
		];
		\Phpcmf\Service::V()->assign('type', $this->type);
	}

	public function index() {

		list($list, $total) = \Phpcmf\Service::M()->table('var')->limit_page();
		
		\Phpcmf\Service::V()->assign([
			'list' => $list,
			'total' => $total,
			'mypages'	=> \Phpcmf\Service::L('Input')->page(\Phpcmf\Service::L('Router')->url('myvar/index'), $total, 'admin')
		]);
		\Phpcmf\Service::V()->display('myvar_index.html');
	}

	public function add() {

		if (IS_AJAX_POST) {
			$data = $this->_validation(0, \Phpcmf\Service::L('Input')->post('data'));
			$data['value'] = $data['value'][$data['type']];
			$rt = \Phpcmf\Service::M()->table('var')->insert($data);
			!$rt['code'] && $this->_json(0, $rt['msg']);
			\Phpcmf\Service::L('Input')->system_log('添加自定义变量: '.$data['name']);
			exit($this->_json(1, dr_lang('操作成功')));
		}

		\Phpcmf\Service::V()->assign([
			'form' => dr_form_hidden()
		]);
		\Phpcmf\Service::V()->display('myvar_add.html');
		exit;
	}


	public function edit() {

		$id = intval(\Phpcmf\Service::L('Input')->get('id'));
		$data = \Phpcmf\Service::M()->table('var')->get($id);
		!$data && exit($this->_json(0, dr_lang('数据#%s不存在', $id)));

		if (IS_AJAX_POST) {
			$data = $this->_validation($id, \Phpcmf\Service::L('Input')->post('data'));
			$data['value'] = $data['value'][$data['type']];
			$rt = \Phpcmf\Service::M()->table('var')->update($id, $data);
			!$rt['code'] && $this->_json(0, $rt['msg']);
			\Phpcmf\Service::L('Input')->system_log('修改自定义变量: '.$data['name']);
			exit($this->_json(1, dr_lang('操作成功')));
		}

		\Phpcmf\Service::V()->assign([
			'data' => $data,
			'form' => dr_form_hidden(),
			'code' => $data['type'] ? "{dr_var_value('".$data['cname']."')}" : ("{if dr_var_value('".$data['cname']."')}".PHP_EOL."是".PHP_EOL."{else}".PHP_EOL."否".PHP_EOL."{/if}")
		]);
		\Phpcmf\Service::V()->display('myvar_add.html');
		exit;
	}

	public function del() {

		$ids = \Phpcmf\Service::L('Input')->get_post_ids();
		!$ids && exit($this->_json(0, dr_lang('你还没有选择呢')));

		$rt = \Phpcmf\Service::M()->table('var')->deleteAll($ids);
		!$rt['code'] && $this->_json(0, $rt['msg']);
		\Phpcmf\Service::L('Input')->system_log('批量删除自定义变量: '. @implode(',', $ids));
		exit($this->_json(1, dr_lang('操作成功'), ['ids' => $ids]));
	}


	// 验证数据
	private function _validation($id, $data) {

		list($data, $return) = \Phpcmf\Service::L('Form')->validation($data, $this->form);
		\Phpcmf\Service::M()->table('var')->is_exists($id, 'cname', $data['cname']) && exit($this->_json(0, dr_lang('别名已经存在'), ['field' => 'cname']));
		$return && exit($this->_json(0, $return['error'], ['field' => $return['name']]));

		return $data;
	}
	
}
