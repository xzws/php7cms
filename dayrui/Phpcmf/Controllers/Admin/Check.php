<?php namespace Phpcmf\Controllers\Admin;

class Check extends \Phpcmf\Common
{

    private $_list = [

        '01' => '文件上传检测',
        '02' => 'PHP环境检测',
        '03' => '目录权限检测',
        '04' => '后台入口名称检测',
        '05' => '数据库权限检测',
        '06' => '模板完整性检测',
        '07' => '数据库表结构检测',
        '08' => '程序兼容性检测',

    ];

	public function index() {

        \Phpcmf\Service::V()->assign([
            'menu' => \Phpcmf\Service::M('auth')->_admin_menu(
                [
                    '系统体检' => [\Phpcmf\Service::L('Router')->class.'/index', 'fa fa-wrench'],
                    'PHP环境' => [\Phpcmf\Service::L('Router')->class.'/php_index', 'fa fa-code'],
                ]
            ),
            'list' => $this->_list,
        ]);
		\Phpcmf\Service::V()->display('check_index.html');
	}

	public function do_index() {

	    $id = $_GET['id'];

	    switch ($id) {

            case '01':

                $post = intval(@ini_get("post_max_size"));
                $file = intval(@ini_get("upload_max_filesize"));

                if ($file > $post) {
                    $this->_json(0,'系统配置不合理，post_max_size值('.$post.')必须大于upload_max_filesize值('.$file.')');
                } elseif ($file < 10) {
                    $this->_json(1,'系统环境只允许上传'.$file.'MB文件，可以设置upload_max_filesize值提升上传大小');
                } elseif ($post < 10) {
                    $this->_json(1,'系统环境要求每次发布内容不能超过'.$post.'MB（含文件），可以设置post_max_size值提升发布大小');
                }

                break;


            case '02':

                if (!function_exists('ini_get')) {
                    $this->_json(0, '系统函数ini_get未启用，将无法获取到系统环境参数');
                } elseif (!function_exists('gzopen')) {
                    $this->halt('zlib扩展未启用，您将无法进行在线升级、无法下载插件等', 0);
                } elseif (!function_exists('gzinflate')) {
                     $this->halt('函数gzinflate未启用，您将无法进行在线升级、无法下载插件等', 0);
                } elseif (!function_exists('fsockopen')) {
                     $this->halt('PHP不支持fsockopen，可能充值接口无法使用、手机短信无法发送、电子邮件无法发送、一键登录无法登录等', 0);
                } elseif (!function_exists('openssl_open')) {
                     $this->halt('PHP不支持openssl，可能充值接口无法使用、手机短信无法发送、电子邮件无法发送、一键登录无法登录等', 0);
                } elseif (!function_exists('curl_init')) {
                     $this->halt('PHP不支持CURL扩展，可能充值接口无法使用、手机短信无法发送、电子邮件无法发送、一键登录无法登录等', 0);
                } elseif (!ini_get('allow_url_fopen')) {
                     $this->halt('allow_url_fopen未启用，远程图片无法保存、网络图片无法上传、可能充值接口无法使用、手机短信无法发送、电子邮件无法发送、一键登录无法登录等', 0);
                }
                break;

            case '03':

                list($thumb_path) = dr_thumb_path();

                $dir = array(
                    WRITEPATH => '无法生成系统缓存文件',
                    WRITEPATH.'data/' => '无法生成系统配置文件，会导致系统配置无效',
                    $thumb_path => '无法生成缩略图缓存文件',
                    SYS_UPLOAD_PATH => '无法上传附件',
                    APPSPATH => '无法创建模块、创建表单',
                    TPLPATH => '无法创建模块模板',
                );

                foreach ($dir as $path => $note) {
                    if (!dr_check_put_path($path)) {
                        $this->_json(0, $note.'【'.$path.'】');
                    }
                }

                break;

            case '04':
                if (SELF == 'admin.php') {
                    $this->halt('为了系统安全，请修改根目录admin.php的文件名', 0);
                }
                break;

            case '05':

                $list = \Phpcmf\Service::M()->db->query('show table status')->getResultArray();
                if (!$list) {
                    $this->halt("无法获取到数据表结构，需要为Mysql账号开启SHOW TABLE STATUS权限", 0);
                }

                $field = \Phpcmf\Service::M()->db->query('SHOW FULL COLUMNS FROM `'.\Phpcmf\Service::M()->dbprefix('admin').'`')->getResultArray();
                if (!$field) {
                    $this->halt("无法通获取到数据表字段结构，需要为Mysql账号开启SHOW FULL COLUMNS权限", 0);
                }

                break;

            case '06':
                if (!is_file(TPLPATH.'pc/'.SITE_TEMPLATE.'/home/index.html')) {
                    $this->halt('网站前端模板【电脑版】不存在：/pc/'.SITE_TEMPLATE.'/home/index.html', 0);
                } elseif (!is_file(TPLPATH.'mobile/'.SITE_TEMPLATE.'/home/index.html')) {
                    $this->halt('网站前端模板【手机版】不存在：/mobile/'.SITE_TEMPLATE.'/home/index.html', 0);
                }
                break;

            case '07':
                $prefix = \Phpcmf\Service::M()->prefix;
                foreach ($this->site as $siteid) {
                    // 升级资料库
                    $table = $prefix.$siteid.'_block';
                    if (\Phpcmf\Service::M()->db->tableExists($table)) {
                        // 创建code字段
                        if (!\Phpcmf\Service::M()->db->fieldExists('code', $table)) {
                            \Phpcmf\Service::M()->query('ALTER TABLE `'.$table.'` ADD `code` VARCHAR(100) NOT NULL');
                        }

                    }
                }

                $table = $prefix.'cron';
                if (!\Phpcmf\Service::M()->db->fieldExists('site', $table)) {
                    \Phpcmf\Service::M()->query('ALTER TABLE `'.$table.'` ADD `site` INT(10) NOT NULL COMMENT \'站点\'');
                }

                $table = $prefix.'member_paylog';
                if (!\Phpcmf\Service::M()->db->fieldExists('site', $table)) {
                    \Phpcmf\Service::M()->query('ALTER TABLE `'.$table.'` ADD `site` INT(10) NOT NULL COMMENT \'站点\'');
                }
/*
                $table = $prefix.'email';
                if (!\Phpcmf\Service::M()->db->tableExists($table)) {
                    \Phpcmf\Service::M()->query('CREATE TABLE IF NOT EXISTS `'.$table.'` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `value` text NOT NULL COMMENT \'配置信息\',
  `displayorder` smallint(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY (`displayorder`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT=\'邮件账户表\';');
                }*/

                /*
                // 创建member_notice username字段
                $table = $prefix.'member_notice';
                if (!\Phpcmf\Service::M()->db->fieldExists('username', $table)) {
                    \Phpcmf\Service::M()->query('ALTER TABLE `'.$table.'` ADD `username` VARCHAR(100) NOT NULL');
                }*/
                break;

            case '08':
                // 程序兼容性
                $local = dr_dir_map(APPSPATH, 1); // 搜索本地模块
                foreach ($local as $dir) {
                    if (is_file(APPSPATH.$dir.'/Config/App.php')) {
                        $key = strtolower($dir);
                        $file =  APPSPATH.$dir.'/Controllers/Search.php';
                        if (is_file($file)) {
                            // 替换搜索控制器
                            $code = file_get_contents($file);
                            if (strpos($code, '\Phpcmf\Home\Search') !== false) {
                                file_put_contents($file, str_replace(
                                    ['\Phpcmf\Home\Search', '_Module_Search'],
                                    ['\Phpcmf\Home\Module', '_Search'],
                                    $code
                                ));
                            }
                        }
                    }
                }
                break;

            case '09':

                break;

        }

        $this->_json(1,'完成');
    }

	public function php_index() {
	    phpinfo();
    }


    private function halt($msg, $code) {
        $this->_json($code, $msg);
    }

}
