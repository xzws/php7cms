<?php namespace Phpcmf\Controllers\Admin;

// URL规则
class Urlrule extends \Phpcmf\Table
{
    public $type;

    public function __construct(...$params)
    {
        parent::__construct(...$params);
        $this->type = array(
            0 => dr_lang('自定义页面'),
            4 => dr_lang('站点URL'),
            1 => dr_lang('独立模块'),
            2 => dr_lang('共享模块'),
            3 => dr_lang('共享栏目'),
            //5 => dr_lang('空间黄页'),
            //6 => dr_lang('会员模块'),
        );
        \Phpcmf\Service::V()->assign([
            'menu' => \Phpcmf\Service::M('auth')->_admin_menu(
                [
                    'URL规则' => [\Phpcmf\Service::L('Router')->class.'/index', 'fa fa-link'],
                    '添加' => [\Phpcmf\Service::L('Router')->class.'/add', 'fa fa-plus'],
                    '修改' => ['hide:'.\Phpcmf\Service::L('Router')->class.'/edit', 'fa fa-edit'],
                ]
            ),
        ]);
        // 支持附表存储
        $this->is_data = 0;
        $this->my_field = array(
            'name' => array(
                'ismain' => 1,
                'name' => dr_lang('名称'),
                'fieldname' => 'name',
                'fieldtype' => 'Text',
                'setting' => array(
                    'option' => array(
                        'width' => 200,
                    ),
                    'validate' => array(
                        'required' => 1,
                    )
                )
            ),
        );
        // url显示名称
        $this->name = dr_lang('URL规则');
        // 初始化数据表
        $this->_init([
            'table' => 'urlrule',
            'field' => $this->my_field,
            'order_by' => 'id desc',
        ]);
    }

    // 后台查看url列表
    public function index() {
        
        $this->_List([], -1);
        \Phpcmf\Service::V()->assign('color', [
            0 => 'default',
            1 => 'info',
            2 => 'success',
            3 => 'warning',
            4 => 'danger',
            5 => '',
            6 => 'primary',
        ]);
        \Phpcmf\Service::V()->display('urlrule_index.html');
    }

    // 后台添加url内容
    public function add() {
        $this->_Post(0);
        \Phpcmf\Service::V()->display('urlrule_add.html');
    }

    // 后台修改url内容
    public function edit() {
        $this->_Post(intval(\Phpcmf\Service::L('Input')->get('id')));
        \Phpcmf\Service::V()->display('urlrule_add.html');
    }

    // 复制url
    public function copy_edit() {

        $id = intval(\Phpcmf\Service::L('Input')->get('id'));
        $data = \Phpcmf\Service::M()->db->table('urlrule')->where('id', $id)->get()->getRowArray();
        !$data && $this->_josn(0, dr_lang('数据#%s不存在', $id));

        unset($data['id']);
        $data['name'].= '_copy';

        $rt = \Phpcmf\Service::M()->table('urlrule')->insert($data);

        !$rt['code'] && $this->_json(0, dr_lang($rt['msg']));
        $this->_json(1, dr_lang('复制成功'));
    }


    // 保存
    protected function _Save($id = 0, $data = [], $old = [], $func = null, $func2 = null) {
        return parent::_Save($id, $data, $old, function($id, $data){
            // 保存前的格式化
            $type = (int)\Phpcmf\Service::L('Input')->post('type');
            $value = \Phpcmf\Service::L('Input')->post('value');
            $data[1]['type'] = $type;
            $value[$type]['catjoin'] = \Phpcmf\Service::L('Input')->post('catjoin') ? \Phpcmf\Service::L('Input')->post('catjoin') : '/';
            $data[1]['value'] = dr_array2string($value[$type]);
            return dr_return_data(1, 'ok', $data);
        });
    }

    /**
     * 获取内容
     * $id      内容id,新增为0
     * */
    protected function _Data($id = 0) {

        $data = parent::_Data($id);
        $data['value'] = dr_string2array($data['value']);
        return $data;
    }

    // 后台删除url内容
    public function del() {
        $this->_Del(\Phpcmf\Service::L('Input')->get_post_ids());
    }

}
