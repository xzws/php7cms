<?php namespace Phpcmf\Controllers\Admin;

// 内容模块tag操作类 基于 Ftable
class Tag extends \Phpcmf\Table
{
    public $pid;

    public function __construct(...$params) {
        parent::__construct(...$params);
        // 支持附表存储
        $this->is_data = 0;
        // 模板前缀(避免混淆)
        $this->tpl_prefix = 'tag_';
        // 单独模板命名
        $this->tpl_name = 'tag_content';
        // 模块显示名称
        $this->name = dr_lang('Tag');
        // pid
        $this->pid = intval(\Phpcmf\Service::L('input')->get('pid'));
        $myfield = [
            'name' => [
                'ismain' => 1,
                'name' => dr_lang('名称'),
                'fieldname' => 'name',
                'fieldtype' => 'Text',
                'setting' => array(
                    'option' => array(
                        'width' => 200,
                    ),
                    'is_right' => 2,
                )
            ],
            'displayorder' => array(
                'name' => dr_lang('权重值'),
                'ismain' => 1,
                'fieldtype' => 'Touchspin',
                'fieldname' => 'displayorder',
                'setting' => array(
                    'option' => array(
                        'width' => 200,
                        'max' => '999999999',
                        'min' => '0',
                        'step' => '1',
                        'show' => '1',
                        'value' => 0
                    ),
                    'validate' => array(
                        'tips' => dr_lang('权重值越高排列越靠前'),
                    )
                )
            ),
            'content' => [
                'ismain' => 1,
                'name' => dr_lang('描述信息'),
                'fieldname' => 'content',
                'fieldtype' => 'Ueditor',
                'setting' => array(
                    'option' => array(
                        'mode' => 1,
                        'height' => 300,
                        'width' => '100%',
                    ),
                )
            ],
        ];
        // 初始化数据表
        $this->_init([
            'table' => SITE_ID.'_tag',
            'field' => dr_array2array($myfield, \Phpcmf\Service::L('cache')->get('tag-'.SITE_ID.'-field')),
            'show_field' => 'name',
            'sys_field' => ['content'],
            'where_list' => 'pid='.$this->pid,
            'order_by' => 'displayorder DESC,id ASC',
        ]);
        \Phpcmf\Service::V()->assign([
            'pid' => $this->pid,
            'menu' => \Phpcmf\Service::M('auth')->_admin_menu(
                [
                    '关键词' => [\Phpcmf\Service::L('Router')->class.'/index', 'fa fa-tag'],
                    '添加' => [\Phpcmf\Service::L('Router')->class.'/add', 'fa fa-plus'],
                    '修改' => ['hide:'.\Phpcmf\Service::L('Router')->class.'/edit', 'fa fa-edit'],
                    '批量添加' => [\Phpcmf\Service::L('Router')->class.'/all_add', 'fa fa-plus-square-o'],
                    '自定义字段' => ['url:'.\Phpcmf\Service::L('Router')->url('field/index', ['rname' => 'tag', 'rid'=>SITE_ID]), 'fa fa-code'],
                    'help' => [63]
                ]
            ),
            'field' => $this->init['field'],
        ]);
    }

    // ========================

    // 后台查看列表
    public function index() {

        list($tpl) = $this->_List();

        \Phpcmf\Service::V()->display($tpl);
    }

    // 后台批量添加内容
    public function all_add() {

        if (IS_AJAX_POST) {
            $rt = \Phpcmf\Service::M('Tag')->save_all_data($this->pid, $_POST['all']);
            $this->_json($rt['code'], $rt['msg']);
        }
        
        \Phpcmf\Service::V()->assign([
            'form' => dr_form_hidden(),
            'reply_url' =>\Phpcmf\Service::L('Router')->get_back(\Phpcmf\Service::L('Router')->uri('index')),
        ]);

        \Phpcmf\Service::V()->display('tag_all.html');
    }

    // 后台添加内容
    public function add() {
        list($tpl) = $this->_Post(0);
        \Phpcmf\Service::V()->assign([
            'form' => dr_form_hidden(),
        ]);
        \Phpcmf\Service::V()->display($tpl);exit;
    }

    // 后台修改内容
    public function edit() {
        list($tpl) = $this->_Post(intval(\Phpcmf\Service::L('Input')->get('id')));
        \Phpcmf\Service::V()->assign([
            'form' => dr_form_hidden(),
        ]);
        \Phpcmf\Service::V()->display($tpl);exit;
    }

    // 后台批量保存排序值
    public function order_edit() {
        $this->_Display_Order(
            intval(\Phpcmf\Service::L('Input')->get('id')),
            intval(\Phpcmf\Service::L('Input')->get('value'))
        );
    }

    // 后台删除内容
    public function del() {
        $this->_Del(
            \Phpcmf\Service::L('Input')->get_post_ids(),
            null,
            null,
            \Phpcmf\Service::M()->dbprefix($this->init['table'])
        );
    }

    // ===========================

    /**
     * 保存内容
     * $id      内容id,新增为0
     * $data    提交内容数组,留空为自动获取
     * $func    格式化提交的数据
     * */
    protected function _Save($id = 0, $data = [], $old = [], $func = null, $func2 = null) {

        return parent::_Save($id, $data, $old,
            function ($id, $data, $old) {
                // 提交之前的判断
                $post = \Phpcmf\Service::L('Input')->post('data');
                if (\Phpcmf\Service::M('Tag')->check_code($id, $post['code'])) {
                    return dr_return_data(0, dr_lang('别名已经存在'));
                } elseif (\Phpcmf\Service::M('Tag')->check_name($id, $post['name'])) {
                    return dr_return_data(0, dr_lang('tag名称已经存在'));
                }
                $data[1]['pid'] = $old ? $old['pid'] : $this->pid;
                !$old && $data[1]['total'] = 0; // 初始化字段
                !$old && $data[1]['childids'] = ''; // 初始化字段
                $data[1]['code'] = $post['code'];
                $data[1]['hits'] = intval($post['hits']);
                !$data[1]['content'] && $data[1]['content'] = '';
                return dr_return_data(1, 'ok', $data);
            },
            function ($id, $data, $old) {
                // 提交之后
                $data[1]['pcode'] = \Phpcmf\Service::M('Tag')->get_pcode($data[1]);
                $data[1]['url'] =\Phpcmf\Service::L('Router')->tag_url($data[1]['pcode']);
                // 更新
                \Phpcmf\Service::M('Tag')->save_data($id, array(
                    'url' => $data[1]['url'],
                    'pcode' => $data[1]['pcode'],
                ));
                return $data;
            }
        );
    }
}
