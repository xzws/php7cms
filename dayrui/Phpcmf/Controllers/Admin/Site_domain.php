<?php namespace Phpcmf\Controllers\Admin;

class Site_domain extends \Phpcmf\Common
{
	public function index() {

		if (IS_AJAX_POST) {
            \Phpcmf\Service::M('Site')->domain(\Phpcmf\Service::L('Input')->post('data', true));
            \Phpcmf\Service::L('Input')->system_log('设置网站域名参数');
			exit($this->_json(1, dr_lang('操作成功')));
		}

        $page = intval(\Phpcmf\Service::L('input')->get('page'));
        list($module, $data) = \Phpcmf\Service::M('Site')->domain();

		\Phpcmf\Service::V()->assign([
			'page' => $page,
			'data' => $data,
			'form' => dr_form_hidden(['page' => $page]),
            'menu' => \Phpcmf\Service::M('auth')->_admin_menu(
                [
                    '域名设置' => ['site_domain/index', 'fa fa-cog'],
                ]
            ),
            'module' => $module
		]);
		\Phpcmf\Service::V()->display('site_domain.html');
	}

	
}
