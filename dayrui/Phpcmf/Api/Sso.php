<?php

/**
 * 单点登录接口
 */

switch (\Phpcmf\Service::L('Input')->get('action')) {

    case 'logout': // 前台退出登录

        header('P3P: CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"');
        \Phpcmf\Service::L('Input')->set_cookie('member_uid', 0, -30000);
        \Phpcmf\Service::L('Input')->set_cookie('member_cookie', 0, -30000);
        break;

    case 'login': // 前台同步登录

        $code = dr_authcode(\Phpcmf\Service::L('Input')->get('code'), 'DECODE');
        !$code && $this->_jsonp(0, '解密失败');

        list($uid, $salt) = explode('-', $code);
        (!$uid || !$salt) && $this->_jsonp(0, '格式错误');

        $member = \Phpcmf\Service::M()->db->table('member')->select('password')->where('id', $uid)->get()->getRowArray();
        !$member && $this->_jsonp(0, '账号不存在');
        $salt != $member['salt'] && $this->_jsonp(0, '账号验证失败');

        header('P3P: CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"');
        $expire = \Phpcmf\Service::L('Input')->get('remember') ? 8640000 : SITE_LOGIN_TIME;
        \Phpcmf\Service::L('Input')->set_cookie('member_uid', $uid, $expire);
        \Phpcmf\Service::L('Input')->set_cookie('member_cookie', substr(md5(SYS_KEY.$member['password']), 5, 20), $expire);

        break;
    
    case 'alogin': // 后台登录授权

        $code = dr_authcode(\Phpcmf\Service::L('Input')->get('code'), 'DECODE');
        !$code && $this->_jsonp(0, '解密失败');

        list($uid, $password) = explode('-', $code);

        $admin = \Phpcmf\Service::L('cache')->get_data('admin_login_member');
        !$admin && $this->_jsonp(0, '缓存失败');

        $mycode = md5($admin['id'].$admin['password']);
        $password != $mycode && $this->_jsonp(0, '验证失败');

        // 存储状态
        header('P3P: CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"');
        \Phpcmf\Service::L('Input')->set_cookie('admin_login_member', $uid.'-'.$admin['id'], 3600);
        $this->session()->set('admin_login_member_code', md5($uid.$admin['id'].$admin['password']), 3600);
        break;



}

$this->_jsonp(1, 'ok');