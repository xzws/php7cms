<?php

/**
 * 运行任务接口
 */

// 未到时间
if (\Phpcmf\Service::L('input')->get_cookie('cron')) {
    exit('未到时间');
}

// 批量执行站点动作
foreach ($this->site_info as $siteid => $site) {
    // 删除网站首页
    if (SYS_CACHE_INDEX && $site['SITE_INDEX_HTML']) {
        \Phpcmf\Service::M('cron')->clear_html_file(SITE_URL, SYS_CACHE_INDEX);
        \Phpcmf\Service::M('cron')->clear_html_file(SITE_MURL, SYS_CACHE_INDEX);
    }
    // 模块
    $module = \Phpcmf\Service::L('cache')->get('module-'.$siteid.'-content');
    if ($module) {
        foreach ($module as $dir => $mod) {
            // 删除模块首页
            if ($mod['is_index_html'] && SYS_CACHE_INDEX) {
                \Phpcmf\Service::M('cron')->clear_html_file(dr_url_prefix($mod['url'], null, $siteid), SYS_CACHE_INDEX);
                \Phpcmf\Service::M('cron')->clear_html_file(dr_url_prefix($mod['murl'], null, $siteid), SYS_CACHE_INDEX);
            }
            // 定时发布动作
            $db = \Phpcmf\Service::M('Content', $dir);
            $db->_init($dir, $siteid);
            $this->module = \Phpcmf\Service::L('cache')->get('module-'.$siteid.'-'.$dir);
            $times = $db->table($siteid.'_'.$dir.'_time')->where('posttime < '.SYS_TIME)->getAll();
            if ($times) {
                foreach ($times as $t) {
                    $rt = $db->post_time($t);
                    if (!$rt['code']) {
                        log_message('error', '定时发布（'.$t['id'].'）失败：'.$rt['msg']);
                    }
                }
            }

        }
    }

}


// 执行队列
$i = \Phpcmf\Service::M('cron')->run_cron();

// 100秒调用本程序
\Phpcmf\Service::L('input')->set_cookie('cron', 1, 100);

// 任务计划
\Phpcmf\Hooks::trigger('cron');

exit('Run '.$i);