<?php namespace Phpcmf\Controllers;

class $NAME$ extends \Phpcmf\Home\Form
{

    public function index() {
        $this->_Home_List();
    }

    public function show() {
        $this->_Home_Show();
    }

    public function post() {
        $this->_Home_Post();
    }

}
