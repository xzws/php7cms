<?php


class Show extends MY_Controller {


    /**
     * 阅读
     */
    public function index() {


        $id = (int)$this->input->get('id');
        $index = $this->db->where('id', $id)->get('index')->row_array();
        if (!$index) {
            $this->goto_404_page(fc_lang('无法通过%s找到对应的模型', $id));
        }
        // 设置模型信息
        $this->dir = $index['mid'];
        if (!$this->dir) {
            $this->goto_404_page(fc_lang('此内容mid参数不存在'));
        }

        $category = $this->category;

        // 正式内容缓存查询结果
        $name = 'show_'.$this->dir.'_'.$id;
        $data = $this->get_cache_data($name);
        if (!$data) {
            $data = $this->db->where('id', $id)->limit(1)->get($this->dir)->row_array();
            if (!$data) {
                $this->goto_404_page(fc_lang('内容(id#%s)不存在', $id));
            }
            $data2 = $this->db->where('id', $id)->limit(1)->get($this->dir.'_data')->row_array();
            $data2 && $data = $data2+$data;

            $data['catid'] = intval($data['catid']);
            $cat = $category[$data['catid']];

            // 上一篇文章
            $this->db->where('catid', $data['catid']);
            $this->db->where('id<', (int)$id);
            $this->db->order_by('id desc');
            $data['prev_page'] = $this->db->limit(1)->get($this->dir)->row_array();

            // 下一篇文章
            $this->db->where('catid', $data['catid']);
            $this->db->where('id>', (int)$id);
            $this->db->order_by('id asc');
            $data['next_page'] = $this->db->limit(1)->get($this->dir)->row_array();

            // 缓存数据
            $data = $this->set_cache_data($name, $data, 3600);
        } else {
            $cat = $category[$data['catid']];
        }

        // 状态判断
        if ($data['status'] != 9) {
            $this->goto_404_page(fc_lang('您暂时无法访问'));
        }

        $myurl = SITE_URL.$data['url'];
        if ($myurl != dr_now_url()) {
            header('Location: '.$myurl, TRUE, 301);
            exit;
        }

        // 栏目下级或者同级栏目
        list($parent, $related) = $this->_related_cat($category, $data['catid']);

        $this->template->assign($data);
        $this->template->assign(dr_show_seo($data));
        $this->template->assign(array(
            'cat' => $cat,
            'top' => $category[$data['catid']]['topid'] && $category[$category[$data['catid']]['topid']] ? $category[$category[$data['catid']]['topid']] : $cat,
            'parent' => $parent,
            'related' => $related,
        ));

        $this->template->module($this->dir);
        $this->template->display('show.html');

    }

}