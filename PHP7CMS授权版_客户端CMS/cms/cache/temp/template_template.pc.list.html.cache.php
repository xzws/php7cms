<?php if ($fn_include = $this->_include("header.html")) include($fn_include); ?>
<div class="pst_bg">
    <div class="pst">
        您当前的位置：

        <?php echo dr_catpos($catid, ' > ', true, '<a href="[url]">[name]</a>'); ?>
    </div>
</div>
<div class="scd clearfix">
    <div class="scd_l">
        <div class="s_name">
            <?php echo $parent['name']; ?>
        </div>
        <ul class="s_nav">
            <?php if (is_array($related)) { $count=count($related);foreach ($related as $t) { ?>
            <li class="<?php if ($t['id']==$catid) { ?>now<?php } ?>"><a href="<?php echo $t['url']; ?>"><?php echo $t['name']; ?></a></li>
            <?php } } ?>
        </ul>
    </div>
    <div class="scd_r">
        <div class="r_name"><span><?php echo $cat['name']; ?></span></div>
        <div class="new">
            <?php $list_return = $this->list_tag("action=module catid=$catid order=updatetime page=1"); if ($list_return) extract($list_return, EXTR_OVERWRITE); $count=count($return); if (is_array($return)) { foreach ($return as $key=>$t) { ?>
            <dl class="clearfix">
                <dt><a href="<?php echo $t['url']; ?>"><img src="<?php echo $t['thumb']; ?>"></a></dt>
                <dd>
                    <div class="title">
                        <a href="<?php echo $t['url']; ?>">
                            <p><?php echo $t['title']; ?></p>
                            <em>[<?php echo dr_date($t['updatetime'], 'Y-m-d'); ?>]</em>
                        </a>
                    </div>
                    <div class="des"><?php echo $t['description']; ?></div>
                </dd>
            </dl>
            <?php } } ?>

            <div class="space_hx">&nbsp;</div>
            <div class="pages">
                <?php echo $pages; ?>
            </div>
        </div>
    </div>
</div>
<?php if ($fn_include = $this->_include("footer.html")) include($fn_include); ?>