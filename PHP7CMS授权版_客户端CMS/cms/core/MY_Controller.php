<?php

class MY_Controller extends CI_Controller {

    public $my;
    public $space;
    public $category;
    public $category_dir;

    public function __construct()
    {
        parent::__construct();

        if (!isset($_GET['nodb'])) {

            $this->load->database();

            $config = require APPPATH.'cache/config.php';
            $this->space = $config['space'];
            $this->category = $config['category'];
            $this->category_dir = $config['category_dir'];

            define('SITE_NAME', $this->space['title']);
            define('SITE_URL', ltrim($this->space['weburl'], '/'));

            define('SITE_TITLE', $this->space['mtitle']);
            define('SITE_KEYWORDS', $this->space['keywords']);
            define('SITE_DESCRIPTION', $this->space['description']);
            //print_r($config);
        }

        define('HOME_THEME_PATH', '/static/');
        define('SITE_SEOJOIN', '_');
        define('IS_MOBILE', 0);

        $this->load->library('template');
        $this->template->ci = $this;
        $this->template->mobile = 0;
        $this->template->assign(array(
            'space' => $this->space,
            'category' => $this->category,
            'flink' => $config['flink'],
        ));

    }

    public function get_cache() {

        $param = func_get_args();
        if (!$param) {
            return NULL;
        }

        // 取第一个参数作为缓存变量名称
        $data = $model = NULL;
        $name = strtolower(array_shift($param));

        exit('get_cache:'.$name);

        if (!$data) {
            $var = 'cache-'.$name;
            if (isset($this->$var) && $this->$var) {
                // 读取全局变量
                $data = $this->$var;
            } else {
                // 读取本地文件缓存数据
                $data = $this->$var = $this->dcache->get($name);
            }
        }

        if (!$param) {
            return $data;
        }

        $var = '';
        foreach ($param as $v) {
            $var.= '[\''.dr_safe_replace($v).'\']';
        }

        $return = NULL;
        @eval('$return = $data'.$var.';');

        return $return;
    }

    // 获取任意表字段
    public function get_table_field($table) {

        $name = 'mytable-'.$table;
        $value = $this->get_cache_data($name);
        if (!$value) {
            $data = $this->db->field_data($table);
            $value = array();
            foreach ($data as $t) {
                $value[$t->name] = $t->name;
            }

            $this->set_cache_data($name, $value, 36000000);
        }

        return $value;
    }


    /**
     * 引用404页面
     */
    public function goto_404_page($msg) {
        //header("status: 404 Not Found");
        $this->template->assign(array(
            'msg' => $msg,
            'meta_title' => $msg
        ));
        $this->template->display('404.html');exit;
    }

    /**
     * 栏目下级或者同级栏目
     */
    protected function _related_cat($category, $catid) {

        if (!$category) {
            return array(NULL, NULL);
        }

        $cat = $category[$catid];
        $related = $parent = array();

        if ($cat['child']) {
            $parent = $cat;
            foreach ($category as $t) {
                $t['pid'] == $cat['id'] && $related[] = $t;
            }
        } elseif ($cat['pid']) {
            foreach ($category as $t) {
                if ($t['pid'] == $cat['pid']) {
                    $related[] = $t;
                    $parent = $cat['child'] ? $cat : $category[$t['pid']];
                }
            }
        } else {
            if (!$category) {
                return array(NULL, NULL);
            }
            $parent = $cat;
            foreach ($category as $t) {
                $t['pid'] == 0 && $related[] = $t;
            }
        }

        return array($parent, $related);
    }


    /**
     * 统一返回json格式并退出程序
     */
    public function _json($code, $msg, $data = array()){

        echo json_encode(dr_return_data($code, $msg, $data));exit;
    }

    public function get_cache_data($name) {

        if (!$name) {
            return NULL;
        }


        return $this->cache->file->get($name);
    }


    public function set_cache_data($name, $data, $ttl = 3600) {

        $ttl = (int)$ttl;

        if (!$name || !$ttl) {
            return $data;
        }

        $this->cache->file->save($name, $data, $ttl);

        return $data;
    }
}

// php 5.5 以上版本的正则替换方法
class php5replace {

    private $data;

    public function __construct($data) {
        $this->data = $data;
    }

    // 替换常量值 for php5.5
    public function php55_replace_var($value) {
        $v = '';
        @eval('$v = '.$value[1].';');
        return $v;
    }

    // 替换数组变量值 for php5.5
    public function php55_replace_data($value) {
        return $this->data[$value[1]];
    }

    // 替换函数值 for php5.5
    public function php55_replace_function($value) {

        if (function_exists($value[1])) {
            if ($value[2] == '$data') {
                $param = $this->data;
            } else {
                $param = $value[2];
            }
            return call_user_func_array($value[1], is_array($param) ? $param : @explode(',', $param));
        }

        return $value[0];
    }

}