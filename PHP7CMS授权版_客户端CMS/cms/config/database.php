<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 数据库配置
 */

$config = require WEBPATH.'config/database.php';

$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => $config['hostname'],
	'username' => $config['username'],
	'password' => $config['password'],
	'database' => $config['database'],
	'dbprefix' => 'dr_',
    'dbdriver' => 'mysqli',
	'pconnect' => FALSE,
	'db_debug' => FALSE,
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => false
);
