<?php

/**
 * 数据库配置文件
 */

$db['default']	= [
    'hostname'	=> 'localhost',
    'username'	=> 'root',
    'password'	=> 'root',
    'database'	=> 'php7cms_git',
    'DBPrefix'	=> 'dr_',
];