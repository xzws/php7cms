<?php

/**
 * Cms 主程序
 */

declare(strict_types=1);
header('Content-Type: text/html; charset=utf-8');

define('IS_DEV', 0);

define('ROOTPATH', dirname(__FILE__).'/');

define('FCPATH', dirname(__FILE__).'/dayrui/');

define('WRITEPATH', ROOTPATH.'cache/');

define('WEBPATH', ROOTPATH);

!defined('SELF') && define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

!defined('IS_API') && define('IS_API', isset($_GET['s']) && $_GET['s'] == 'api');

!defined('IS_ADMIN') && define('IS_ADMIN', FALSE);

IS_ADMIN || IS_DEV ? error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_STRICT) : error_reporting(0);

require FCPATH.'Fcms/Init.php';